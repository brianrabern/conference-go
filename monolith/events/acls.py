import requests
import math
from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_weather(city, state):
    # get lat and lon
    params1 = {"q": f"{city},{state},US", "appid": OPEN_WEATHER_API_KEY}
    url1 = "http://api.openweathermap.org/geo/1.0/direct"
    geo = requests.get(url1, params=params1)
    geo_data = geo.json()
    lat = geo_data[0]["lat"]
    lon = geo_data[0]["lon"]

    # temp and weather desciption
    url2 = "https://api.openweathermap.org/data/2.5/weather"
    params2 = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    stuff = requests.get(url2, params=params2)
    data = stuff.json()
    temp = data["main"]["temp"]
    desc = data["weather"][0]["description"]

    return {"tempurature": temp, "description": desc}


def get_location_photo(city, state):
    """ha"""
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    resp = requests.get(url, headers=headers)
    data = resp.json()
    location_pic = data["photos"][0]["url"]
    return location_pic


def get_fact(num):
    z = math.floor(num)
    url = f"http://numbersapi.com/{z}?json"
    x = requests.get(url)
    a = x.json()
    d = a["text"]

    return d
